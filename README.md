# Drop Token Game

The drop-token project is an implementation of a simple game where players take turns dropping tokens into a square board of slots until someone has created a continuous line (vertical, horizontal or diagonal) of four of their tokens.

The project utilizes create-react-app to simplify configuration. TypeScript is employed over vanilla JavaScript for its static type system and the developer tools that it enables. React is used for its ability to fluidly mix markup declaration and UI code, while Redux (specifically @reduxjs/toolkit) is used to better manage centralized state. The app attempts to follow the traditional "dumb" vs "smart" component dichotomy to simplify testing (component re-usability is also a plus, but not terribly important here). Tests are written with the built-in Jest and @testing-library/react dependencies.

SASS is employed over basic CSS for additional convenient syntax, and the project also uses CSS modules to simplify global cascade management. No styling frameworks are used, as the app is quite simple in appearance and functionality, and all styles in this case are easy to declare by hand. Font Awesome is used for icons, however.

The only dependency is yarn (or npm, which can install yarn with `npm install -g yarn`). Visual Studio Code was used for development, though any text editor/IDE with access to a command line will work.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
