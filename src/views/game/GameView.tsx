import React, { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Board from '../../components/board/Board';
import BoardHeader from '../../components/board-header/BoardHeader';
import BoardFooter from '../../components/board-footer/BoardFooter';
import FirstPlayerPrompt from '../../components/first-player-prompt/FirstPlayerPrompt';
import { actions, selectors } from '../../redux/store';
import useGameAI from '../../services/GameAIService';
import { calculateBoardProgress, checkForFullBoard } from '../../util/game';
import styles from './GameView.module.scss';

/**
 * Smart component for rendering the game state. Also somewhat of a game state manager
 * as it responds to state changes and triggers async actions as needed
 */
const GameView = () => {
  const dispatch = useDispatch();
  const { fetchAIMove } = useGameAI();
  const {
    activePlayerId,
    firstPlayerId,
    loading,
    moves,
    settings,
    winnerId,
  } = useSelector(selectors.selectGame);
  const { height, players, width } = settings;

  const activePlayer = settings.players.find(player => player.id === activePlayerId);
  if (activePlayerId && !activePlayer) {
    throw new Error(`Invalid active player id: ${activePlayerId}`);
  }

  const winningPlayer = settings.players.find(player => player.id === winnerId);

  useEffect(
    () => {
      const getAIMove = async () => {
        dispatch(actions.game.setLoading(true));

        const aiMove = await fetchAIMove({ moves });

        dispatch(actions.game.addMove({ move: aiMove, loading: false }));
      };

      if (activePlayer?.local === false && winnerId == null) {
        getAIMove();
      }
    },
    [
      activePlayer,
      dispatch,
      fetchAIMove,
      moves,
      winnerId
    ],
  );

  // Get derived board calculations
  const [boardProgress, fullBoard] = useMemo(
    () => {
      if (!firstPlayerId) {
        return [null, false];
      }

      const boardProgress = calculateBoardProgress({
        firstPlayerId,
        height,
        moves,
        players,
        width
      });
      const fullBoard = checkForFullBoard({ boardProgress });

      return [boardProgress, fullBoard];
    },
    [firstPlayerId, height, moves, players, width]
  );

  return (
    <div className={styles['game-view']}>
      {activePlayerId == null
        ? <FirstPlayerPrompt
          onResponse={response => {
            dispatch(actions.game.setFirstPlayer(
              response === 'yes'
                ? players[0]
                : players[1]
            ))
          }}
        />
        : <>
          <BoardHeader
            activePlayer={activePlayer!}
            loading={loading}
            stalemate={fullBoard}
            winnerName={winningPlayer?.name}
          />
          <Board
            boardProgress={boardProgress!}
            onSlotClick={(x, y) => {
              if (loading) {
                return;
              }

              dispatch(actions.game.addMove({ move: x }))
            }}
          />
          <BoardFooter
            onResetClick={() => {
              if (loading) {
                return;
              }

              dispatch(actions.game.reset());
            }}
            gameOver={!!winningPlayer || fullBoard}
          />
        </>
      }
    </div>
  );
};

export default GameView;
