export type GameSettings = {
  height: number;
  // NOTE: Player declaration order also implicitly sets turn order (not necessarily who goes first, though)
  // NOTE: Assume only two players
  players: PlayerSettings[];
  width: number;
}

export type PlayerSettings = {
  color: string;
  id: number;
  local: boolean;
  name: string;
}

export type GameMoves = number[];

export type BoardProgress = { color?: string; playerId?: number; }[][];
