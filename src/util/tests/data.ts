import { BoardProgress, PlayerSettings } from '../../types/game';

export const defaultBoardHeight = 4;
export const defaultBoardWidth = 4;
export const players: PlayerSettings[] = [
  { color: '#000000', id: 1, local: true, name: 'Player 1' },
  { color: '#ffffff', id: 2, local: false, name: 'Player 2' },
];
export const playerMap = players.reduce(
  (map, player) => ({ ...map, [player.id]: player }),
  {} as { [playerId: number]: PlayerSettings }
);

export const movesEmpty = [];
export const movesPartial = [2, 1, 2, 0, 0];
export const movesWinner = [1, 2, 1, 0, 1, 3, 1];
export const movesOverflow = [1, 2, 1, 0, 1, 3, 1, 3, 1];

// Boards

export const boardEmpty: BoardProgress = [
  [ {}, {}, {}, {} ],
  [ {}, {}, {}, {} ],
  [ {}, {}, {}, {} ],
  [ {}, {}, {}, {} ],
];

export const boardPartiallyFilled: BoardProgress = [
  [ {}, {}, {}, {} ],
  [ {}, {}, {}, {} ],
  [ { playerId: 1 }, {}, {}, { playerId: 1 } ],
  [ { playerId: 1 }, { playerId: 2 }, { playerId: 2 }, {} ],
];

export const boardWinnerPlayer1: BoardProgress = [
  [ { playerId: undefined }, { playerId: 1 }, { playerId: undefined }, { playerId: undefined } ],
  [ { playerId: undefined }, { playerId: 1 }, { playerId: undefined }, { playerId: undefined } ],
  [ { playerId: undefined }, { playerId: 1 }, { playerId: undefined }, { playerId: undefined } ],
  [ { playerId: 2 }, { playerId: 1 }, { playerId: 2 }, { playerId: 2 } ],
];

export const boardWinnerPlayer2: BoardProgress = [
  [ { playerId: undefined }, { playerId: 2 }, { playerId: undefined }, { playerId: undefined } ],
  [ { playerId: undefined }, { playerId: 2 }, { playerId: undefined }, { playerId: undefined } ],
  [ { playerId: 1 }, { playerId: 2 }, { playerId: undefined }, { playerId: undefined } ],
  [ { playerId: 1 }, { playerId: 2 }, { playerId: 1 }, { playerId: 1 } ],
];

export const boardWinnerHorizontal: BoardProgress = [
  [ { playerId: undefined }, { playerId: 2 }, { playerId: undefined }, { playerId: undefined } ],
  [ { playerId: undefined }, { playerId: 2 }, { playerId: undefined }, { playerId: undefined } ],
  [ { playerId: undefined }, { playerId: 2 }, { playerId: undefined }, { playerId: undefined } ],
  [ { playerId: 1 }, { playerId: 1 }, { playerId: 1 }, { playerId: 1 } ],
];

export const boardWinnerVertical: BoardProgress = [
  [ { playerId: undefined }, { playerId: 1 }, { playerId: undefined }, { playerId: undefined } ],
  [ { playerId: undefined }, { playerId: 1 }, { playerId: undefined }, { playerId: undefined } ],
  [ { playerId: undefined }, { playerId: 1 }, { playerId: undefined }, { playerId: undefined } ],
  [ { playerId: 2 }, { playerId: 1 }, { playerId: 2 }, { playerId: 2 } ],
];

export const boardWinnerDownDiagonal: BoardProgress = [
  [ { playerId: 1 }, { playerId: 1 }, { playerId: undefined }, { playerId: undefined } ],
  [ { playerId: 2 }, { playerId: 1 }, { playerId: 2 }, { playerId: undefined } ],
  [ { playerId: 1 }, { playerId: 2 }, { playerId: 1 }, { playerId: undefined } ],
  [ { playerId: 2 }, { playerId: 1 }, { playerId: 2 }, { playerId: 1 } ],
];

export const boardWinnerUpDiagonal: BoardProgress = [
  [ { playerId: 2 }, { playerId: 2 }, { playerId: undefined }, { playerId: 1 } ],
  [ { playerId: 1 }, { playerId: 1 }, { playerId: 1 }, { playerId: undefined } ],
  [ { playerId: 2 }, { playerId: 1 }, { playerId: 2 }, { playerId: undefined } ],
  [ { playerId: 1 }, { playerId: 2 }, { playerId: 2 }, { playerId: 2 } ],
];

export const boardFullWinner: BoardProgress = [
  [ { playerId: 1 }, { playerId: 2 }, { playerId: 1 }, { playerId: 2 } ],
  [ { playerId: 2 }, { playerId: 2 }, { playerId: 1 }, { playerId: 1 } ],
  [ { playerId: 1 }, { playerId: 2 }, { playerId: 1 }, { playerId: 2 } ],
  [ { playerId: 2 }, { playerId: 2 }, { playerId: 2 }, { playerId: 2 } ],
];

export const boardFullNoWinner: BoardProgress = [
  [ { playerId: 1 }, { playerId: 2 }, { playerId: 1 }, { playerId: 2 } ],
  [ { playerId: 2 }, { playerId: 2 }, { playerId: 1 }, { playerId: 1 } ],
  [ { playerId: 1 }, { playerId: 2 }, { playerId: 1 }, { playerId: 2 } ],
  [ { playerId: 2 }, { playerId: 1 }, { playerId: 2 }, { playerId: 1 } ],
];
