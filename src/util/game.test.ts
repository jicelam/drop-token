import { BoardProgress } from '../types/game';
import {
  calculateBoardProgress,
  checkForFullBoard,
  checkForGameVictory,
} from './game';
import {
  boardEmpty,
  boardFullNoWinner,
  boardFullWinner,
  boardPartiallyFilled,
  boardWinnerDownDiagonal,
  boardWinnerHorizontal,
  boardWinnerPlayer1,
  boardWinnerPlayer2,
  boardWinnerUpDiagonal,
  boardWinnerVertical,
  defaultBoardHeight,
  defaultBoardWidth,
  movesEmpty,
  movesOverflow,
  movesPartial,
  movesWinner,
  players,
  playerMap
} from './tests/data';

describe('Tests for the calculateBoardProgress', () => {
    test('it should return an empty board for an empty move set', () => {
      const board = calculateBoardProgress({
        firstPlayerId: players[0].id,
        height: defaultBoardHeight,
        moves: movesEmpty,
        players: players,
        width: defaultBoardWidth,
      });

      for (const column of board) {
        for (const slot of column) {
          expect(slot.color).toBeUndefined();
          expect(slot.playerId).toBeUndefined();
        }
      }
    });

  test('it should return a board with the right dimensions', () => {
    const board = calculateBoardProgress({
      firstPlayerId: players[0].id,
      height: defaultBoardHeight,
      moves: movesPartial,
      players: players,
      width: defaultBoardWidth,
    });

    expect(board.length).toEqual(defaultBoardWidth);
    for (const column of board) {
      expect(column.length).toEqual(defaultBoardHeight);
    }
  });

  test('it should fill in the correct number of moves', () => {
    const moves = movesPartial;
    const board = calculateBoardProgress({
      firstPlayerId: players[0].id,
      height: defaultBoardHeight,
      moves: moves,
      players: players,
      width: defaultBoardWidth,
    });

    let filledSlotCount = 0;
    for (const column of board) {
      for (const slot of column) {
        if (slot.playerId != null) {
          filledSlotCount++;
        }
      }
    }

    expect(filledSlotCount).toEqual(moves.length);
  });

  test('it should fill in the appropriate slots on the board based on the given moves', () => {
    const moves = [1, 2, 1, 0, 0];
    const expectedBoard: BoardProgress = [
      [ {}, {}, { color: playerMap[1].color, playerId: 1 }, { color: playerMap[2].color, playerId: 2 } ],
      [ {}, {}, { color: playerMap[1].color, playerId: 1 }, { color: playerMap[1].color, playerId: 1 } ],
      [ {}, {}, {}, { color: playerMap[2].color, playerId: 2 } ],
      [ {}, {}, {}, {} ],
    ];
    const board = calculateBoardProgress({
      firstPlayerId: players[0].id,
      height: defaultBoardHeight,
      moves: moves,
      players: players,
      width: defaultBoardWidth,
    });

    expect(board.length).toEqual(expectedBoard.length);
    for (let x = 0; x < board.length; x++) {
      const column = board[x];
      const expectedColumn = expectedBoard[x];

      expect(column.length).toEqual(expectedColumn.length);
      for (let y = 0; y < column.length; y++) {
        const slot = column[y];
        const expectedSlot = expectedColumn[y];

        expect(slot.color).toEqual(expectedSlot.color);
        expect(slot.playerId).toEqual(expectedSlot.playerId);
      }
    }
  });

  // TODO: Test other inputs more thoroughly
});

describe('Tests for the checkForFullBoard function', () => {
  test('it should return false on an empty board', () => {
    const fullBoard = checkForFullBoard({ boardProgress: boardEmpty });

    expect(fullBoard).toEqual(false);
  });

  test('it should return false on a partial board', () => {
    const fullBoard = checkForFullBoard({ boardProgress: boardPartiallyFilled });

    expect(fullBoard).toEqual(false);
  });

  test('it should return true on a full board with no winner', () => {
    const fullBoard = checkForFullBoard({ boardProgress: boardFullNoWinner });

    expect(fullBoard).toEqual(true);
  });

  test('it should return true on a full board with a winner', () => {
    const fullBoard = checkForFullBoard({ boardProgress: boardFullWinner });

    expect(fullBoard).toEqual(true);
  });
});

describe('Tests for the checkForGameVictory function', () => {
  test('it should return null for an empty board', () => {
    const winnerId = checkForGameVictory({ boardProgress: boardEmpty });

    expect(winnerId).toBeNull();
  });

  test('it should return null for a partial board with no winner', () => {
    const winnerId = checkForGameVictory({ boardProgress: boardPartiallyFilled });

    expect(winnerId).toBeNull();
  });

  test('it should return null for a full board with no winner', () => {
    const winnerId = checkForGameVictory({ boardProgress: boardFullNoWinner });

    expect(winnerId).toBeNull();
  });

  test('it should return the appropriate winning player\'s id', () => {
    const player1WinnerId = checkForGameVictory({ boardProgress: boardWinnerPlayer1 });
    const player2WinnerId = checkForGameVictory({ boardProgress: boardWinnerPlayer2 });

    expect(player1WinnerId).toEqual(1);
    expect(player2WinnerId).toEqual(2);
  });

  test('it should return a winner id for a horizontal win', () => {
    const winnerId = checkForGameVictory({ boardProgress: boardWinnerHorizontal });

    expect(winnerId).not.toBeNull();
  });

  test('it should return a winner id for a vertical win', () => {
    const winnerId = checkForGameVictory({ boardProgress: boardWinnerVertical });

    expect(winnerId).not.toBeNull();
  });

  test('it should return a winner id for a down diagonal win', () => {
    const winnerId = checkForGameVictory({ boardProgress: boardWinnerDownDiagonal });

    expect(winnerId).not.toBeNull();
  });

  test('it should return a winner id for an up diagonal win', () => {
    const winnerId = checkForGameVictory({ boardProgress: boardWinnerUpDiagonal });

    expect(winnerId).not.toBeNull();
  });
});
