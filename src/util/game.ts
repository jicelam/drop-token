import { BoardProgress, GameMoves, PlayerSettings } from '../types/game';

export type CalculateBoardProgress = {
  firstPlayerId: number;
  height: number;
  moves: GameMoves;
  players: PlayerSettings[];
  width: number;
};

/**
 * Accepts game information (height, width, moves, etc.) and transforms it
 * into a multi-dimensional array of slot objects that represents the current
 * game state and is easily digestible by other parts of the application 
 */
export const calculateBoardProgress = ({
  firstPlayerId,
  height,
  moves,
  players,
  width,
}: CalculateBoardProgress): BoardProgress => {
  const result: BoardProgress = new Array(width)
    .fill(0)
    .map(() => new Array(height).fill(0).map(() => ({})));

  const firstPlayerIndex = players.findIndex(player => player.id === firstPlayerId);
  if (firstPlayerIndex < 0) {
    throw new Error(`Player not found with given firstPlayerId: ${firstPlayerIndex}`);
  }

  const emptyRowIndexes = new Array(width)
    .fill(height - 1); // -1 since 0 indexed
  for (let i = 0; i < moves.length; i++) {
    const activePlayer = players[(firstPlayerIndex + i) % players.length];
    const targetColumn = moves[i];
    if (targetColumn < 0 || targetColumn > width - 1) {
      throw new Error(`Invalid move: ${targetColumn}`);
    }

    const targetRow = emptyRowIndexes[targetColumn];
    if (targetRow < 0) {
      // Just assume a move that goes past the top row is ignored
      continue;
    }

    const slot = result[targetColumn][targetRow];
    slot.color = activePlayer.color;
    slot.playerId = activePlayer.id;
    emptyRowIndexes[targetColumn]--;
  }

  return result;
};

export type CheckForFullBoardParams = {
  boardProgress: BoardProgress;
}

/** Checks if all slots on the board have been claimed. True if yes, false if no */
// NOTE: Do as a separate function rather than trying to combine with the victory
// check for separation of concerns, easier testing and the fact that the input
// space will not be that large
export const checkForFullBoard = ({ boardProgress }: CheckForFullBoardParams): boolean => {
  for (let x = 0; x < boardProgress.length; x++) {
    const column = boardProgress[x];

    for (let y = 0; y < column.length; y++) {
      const slot = boardProgress[x][y];

      if (!slot.playerId) {
        return false;
      }
    }
  }

  return true;
}

export type CheckForGameVictoryParams = {
  boardProgress: BoardProgress;
};

/** Checks if the game has a winner. If so, returns that winners id. Returns null if none found */
// NOTE: With 4x4 boards, you really only need to check the verticals for the top row
// horizontals for the left row, and the two diagonals. Implementing a more generic solution
// isn't that much more difficult however, and will be performant given the small input
// size
export const checkForGameVictory = ({ boardProgress }: CheckForGameVictoryParams): number | null => {
  const winningAmount = 4;
  for (let x = 0; x < boardProgress.length; x++) {
    const column = boardProgress[x];

    for (let y = 0; y < column.length; y++) {
      const slot = column[y];
      if (slot.playerId == null) {
        // Unclaimed, so don't bother
        continue;
      }

      let winnerId: number | null = null;
      const checkStrategyParams: CheckStrategyParams = {
        boardProgress,
        x,
        y,
        winningAmount
      };

      winnerId = checkHorizontal(checkStrategyParams);
      if (winnerId != null) {
        return winnerId;
      }

      winnerId = checkVertical(checkStrategyParams);
      if (winnerId != null) {
        return winnerId;
      }

      winnerId = checkDownDiagonal(checkStrategyParams);
      if (winnerId != null) {
        return winnerId;
      }

      winnerId = checkUpDiagonal(checkStrategyParams);
      if (winnerId != null) {
        return winnerId;
      }
    }
  }
  
  return null;
}

type CheckStrategyParams = {
  boardProgress: BoardProgress;
  x: number;
  y: number;
  winningAmount: number;
}

const checkHorizontal = ({
  boardProgress,
  x,
  y,
  winningAmount
}: CheckStrategyParams): number | null => {
  // Don't bother if go over the edge of the board
  if (x + winningAmount > boardProgress.length) {
    return null;
  }

  const slot = boardProgress[x][y];
  const targetPlayerId = slot.playerId;
  for (let xd = 0; xd < winningAmount; xd++) {

    const nextSlot = boardProgress[x + xd][y];
    if (nextSlot.playerId !== targetPlayerId) {
      return null;
    }
  }

  return targetPlayerId!;
};

const checkVertical = ({
  boardProgress,
  x,
  y,
  winningAmount
}: CheckStrategyParams): number | null => {
  // Don't bother if go over the edge of the board
  if (y + winningAmount > boardProgress[x].length) {
    return null;
  }

  const slot = boardProgress[x][y];
  const targetPlayerId = slot.playerId;
  for (let yd = 0; yd < winningAmount; yd++) {
    const nextSlot = boardProgress[x][y + yd];
    if (nextSlot.playerId !== targetPlayerId) {
      return null;
    }
  }

  return targetPlayerId!;
}

const checkDownDiagonal = ({
  boardProgress,
  x,
  y,
  winningAmount
}: CheckStrategyParams): number | null => {
  // Don't bother if go over the edge of the board
  if (
    x + winningAmount > boardProgress.length ||
    y + winningAmount > boardProgress[x].length
  ) {
    return null;
  }

  const slot = boardProgress[x][y];
  const targetPlayerId = slot.playerId;
  for (let xyd = 0; xyd < winningAmount; xyd++) {
    const nextSlot = boardProgress[x + xyd][y + xyd];
    if (nextSlot.playerId !== targetPlayerId) {
      return null;
    }
  }

  return targetPlayerId!;
}

const checkUpDiagonal = ({
  boardProgress,
  x,
  y,
  winningAmount
}: CheckStrategyParams): number | null => {
  // Don't bother if go over the edge of the board
  if (
    x + winningAmount > boardProgress.length ||
    y - winningAmount + 1 < 0 // + 1 for zero-index, of course
  ) {
    return null;
  }

  const slot = boardProgress[x][y];
  const targetPlayerId = slot.playerId;
  for (let xyd = 0; xyd < winningAmount; xyd++) {
    const nextSlot = boardProgress[x + xyd][y - xyd];
    if (nextSlot.playerId !== targetPlayerId) {
      return null;
    }
  }

  return targetPlayerId!;
};
