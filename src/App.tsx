import React from 'react';
import GameView from './views/game/GameView';
import styles from './App.module.scss';

const App = () => (
  <div className={styles['app']}>
    <GameView />
  </div>
);

export default App;
