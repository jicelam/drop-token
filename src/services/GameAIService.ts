import { useCallback } from 'react';
import { GameMoves } from '../types/game';

const endpointUrl = 'https://w0ayb2ph1k.execute-api.us-west-2.amazonaws.com/production'; // TODO: Move to config file

export type FetchAIMoveParams = {
  moves: GameMoves;
}

const useGameAI = () => {
  const fetchAIMove = useCallback(
    async ({ moves }: FetchAIMoveParams): Promise<number> => {
      const response = await fetch(`${endpointUrl}?moves=[${moves.toString()}]`);
      const newMoves = await response.json() as GameMoves;

      return newMoves[newMoves.length - 1];
    },
    []
  );

  return {
    fetchAIMove,
  };
};

export default useGameAI;
