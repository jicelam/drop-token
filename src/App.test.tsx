import { render, screen } from '@testing-library/react';
import React from 'react';
import { Provider } from 'react-redux';
import App from './App';
import store from './redux/store';

test('renders learn react link', () => {
  const elem = render(
    <Provider store={store}>
      <App />
    </Provider>
  );

  expect(elem).toBeDefined();
});
