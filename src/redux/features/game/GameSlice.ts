import { createSlice } from '@reduxjs/toolkit';
import { GameMoves, GameSettings, PlayerSettings } from '../../../types/game';
import { calculateBoardProgress, checkForGameVictory } from '../../../util/game';

export type GameState = {
  activePlayerId: number | null;
  firstPlayerId: number | null; // Can technically be derived from the active player and moves, but much more convenient to declare here
  loading: boolean;
  moves: GameMoves;
  settings: GameSettings;
  winnerId: number | null;
};

export const gameSlice = createSlice({
  name: 'game',
  initialState: {} as GameState,
  reducers: {
    addMove: (
      state,
      action: { type: string, payload: { move: number; loading?: boolean; } },
    ) => {
      const { activePlayerId, firstPlayerId, moves, settings, winnerId } = state;
      const { height, players, width } = settings;
      const { move, loading } = action.payload;

      if (winnerId != null) {
        // Don't add moves if the game is already over
        return;
      }

      if (!firstPlayerId) {
        throw new Error('Invalid state: firstPlayerId has not been set yet');
      }

      const movesInColumn = moves.reduce((total, previousMove) => previousMove === move ? total + 1 : total, 0);
      if (movesInColumn >= height) {
        // Ignore attempts to make a move in a column that's already full
        return;
      }
      
      const activePlayerIdIndex = players.findIndex(player => player.id === activePlayerId);
      if (activePlayerIdIndex < 0) {
        throw new Error(`Invalid state: activePlayerId ${activePlayerId} not found in player list`);
      }
      const nextActivePlayerIdIndex = (activePlayerIdIndex + 1) % players.length;

      state.moves.push(move);

      const boardProgress = calculateBoardProgress({
        firstPlayerId,
        height,
        moves,
        players,
        width
      });

      // Check for winner
      const newWinnerId = checkForGameVictory({ boardProgress});
      if (newWinnerId != null) {
        state.winnerId = newWinnerId;
      } else {
        state.activePlayerId = players[nextActivePlayerIdIndex].id;
      }

      // Set loading if applicable
      if (loading != null) {
        state.loading = loading;
      }
    },
    reset: state => {
      state.activePlayerId = null;
      state.firstPlayerId = null;
      state.loading = false;
      state.moves = [];
      state.winnerId = null;
    },
    setFirstPlayer: (
      state,
      action: { payload: PlayerSettings; type: string; },
    ) => {
      if (state.firstPlayerId != null) {
        return;
      }

      const playerId = action.payload.id;
      state.activePlayerId = playerId;
      state.firstPlayerId = playerId;
    },
    setLoading: (
      state,
      action: { type: string, payload: boolean; },
    ) => {
      state.loading = action.payload;
    },
  },
});

export const selectGame = (state: { game: GameState }) => state.game;

export const actions = gameSlice.actions;

export default gameSlice.reducer;
