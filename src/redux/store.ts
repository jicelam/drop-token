import { configureStore } from '@reduxjs/toolkit';
import { PlayerSettings } from '../types/game';
import gameReducer, { actions as gameActions, selectGame } from './features/game/GameSlice';

export const actions = {
  game: gameActions,
};

export const selectors = {
  selectGame,
};

const players: PlayerSettings[] = [
  {
    color: '#ff0000',
    id: 1,
    local: true,
    name: 'Player 1',
  },
  {
    color: '#0000ff',
    id: 2,
    local: false,
    name: 'Player 2',
  }
];

export default configureStore({
  // TODO: Pull from local/session storage if want to preserve across refreshes
  preloadedState: {
    game: {
      activePlayerId: null,
      moves: [],
      settings: {
        height: 4,
        players: players,
        width: 4,
      },
      winnerId: null
    },
  },
  reducer: {
    game: gameReducer,
  }
});
