import React from 'react';
import styles from './Slot.module.scss';

export type SlotProps = {
  color?: string;
  onClick: () => void;
}

const Slot = ({ color, onClick }: SlotProps) => {
  return (
    <div
      className={styles['slot']}
      onClick={onClick}
      style={{ backgroundColor: color }}
    />
  )
};

export default Slot;
