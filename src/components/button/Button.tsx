import classnames from 'classnames';
import React from 'react';
import styles from './Button.module.scss';

export type ButtonProps = React.HTMLAttributes<HTMLButtonElement> & {
  color: ButtonColor;
};

export type ButtonColor =
  | 'rose'
  | 'royal';

const Button = ({ className, color, ...props }: ButtonProps) => {
  return (
    <button
      {...props}
      className={classnames(
        styles['button'],
        styles[`theme-${color}`],
        className
      )}
      type="button"
    />
  )
}

export default Button;
