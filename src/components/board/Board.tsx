import React from 'react';
import { BoardProgress } from '../../types/game';
import Slot from '../slot/Slot';
import styles from './Board.module.scss';

export type BoardProps = {
  boardProgress: BoardProgress;
  onSlotClick: (x: number, y: number) => void;
}

const Board = ({
  boardProgress,
  onSlotClick,
}: BoardProps) => {
  const width = boardProgress.length;
  const height = boardProgress[0]?.length ?? 0;
  
  return (
    <div className={styles['board']}>
      {new Array(height).fill(0).map((_, y) => (
        <div className={styles['board-row']}>
          {new Array(width).fill(0).map((_, x) => (
            <div className={styles['board-column']}>
              <Slot
                color={boardProgress[x][y].color}
                onClick={() => onSlotClick(x, y)}
              />
            </div>
          ))}
        </div>
      ))}
    </div>
  )
};

export default Board;
