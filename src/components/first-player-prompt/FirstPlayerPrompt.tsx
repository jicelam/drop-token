import React from 'react';
import Button from '../button/Button';
import styles from './FirstPlayerPrompt.module.scss';

export type FirstPlayerPromptProps = {
  onResponse: (response: PromptResponse) => void;
};

export type PromptResponse = 'yes' | 'no';

const FirstPlayerPrompt = ({ onResponse }: FirstPlayerPromptProps) => {
  return (
    <div className={styles['first-player-prompt']}>
      <div className={styles['prompt-text-row']}>
        Would you like to go first?
      </div>
      <div className={styles['button-row']}>
        <Button
          className={styles['prompt-button']}
          color="royal"
          onClick={() => onResponse('yes')}
        >
          Yes
        </Button>
        <Button
          className={styles['prompt-button']}
          color="rose"
          onClick={() => onResponse('no')}
        >
          No
        </Button>
      </div>
    </div>
  );
}

export default FirstPlayerPrompt;
