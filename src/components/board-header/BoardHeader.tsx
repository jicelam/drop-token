import React from 'react';
import { LoaderIcon } from '../../components/loader/Loader';
import { PlayerSettings } from '../../types/game';
import styles from './BoardHeader.module.scss';

export type BoardHeaderProps = {
  activePlayer: PlayerSettings;
  loading: boolean;
  stalemate?: boolean;
  winnerName?: string;
};

const BoardHeader = ({ activePlayer, loading, stalemate, winnerName }: BoardHeaderProps) => {
  return (
    <header className={styles['board-header']}>
      <span className={styles['player-name']}>
        {winnerName
          ? `${winnerName} wins!`
          : stalemate
            ? 'The game is a draw'
            : activePlayer.name
        }
        <LoaderIcon
          className={styles['loader-icon']}
          style={{
            visibility: !activePlayer.local && loading
              ? 'visible'
              : 'hidden'
          }}
        />
      </span>
    </header>
  );
}

export default BoardHeader;
