import React from 'react';
import Button from '../../components/button/Button';
import styles from './BoardFooter.module.scss';

export type BoardFooterProps = {
  gameOver?: boolean;
  onResetClick: () => void;
};

const BoardFooter = ({ onResetClick, gameOver }: BoardFooterProps) => {
  return (
    <div className={styles['board-footer']}>
      <div className={styles['button-row']}>
        <Button
          className={styles['reset-button']}
          color="rose"
          onClick={onResetClick}
        >
          {gameOver
            ? 'Play Again'
            : 'Reset'
          }
        </Button>
      </div>
    </div>
  )
};

export default BoardFooter;
