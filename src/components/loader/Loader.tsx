import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import classnames from 'classnames';
import React from 'react';
import styles from './Loader.module.scss';

export type LoaderIconProps = {
  className?: string;
  style?: React.CSSProperties;
}

export const LoaderIcon = ({ className, style }: LoaderIconProps) => {
  return (
    <div
      className={classnames(styles['loader-icon'], className)}
      style={style}
    >
      <FontAwesomeIcon
        icon="circle-notch"
        size="xs"
        spin
      />
    </div>
  );
};
